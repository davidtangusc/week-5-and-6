let postsTemplate = $('template#posts-template').html();
let renderPosts = Handlebars.compile(postsTemplate);

let postTemplate = $('template#post-template').html();
let renderPost = Handlebars.compile(postTemplate);

let postList;

$('#post-button').on('click', function() {
  let postBody = $('textarea').val();
  $.ajax({
    type: 'post',
    url: 'http://localhost:3000/api/posts',
    data: {
      body: postBody,
      createdAt: new Date()
    }
  }).then(function(response) {
    postList.push(response);
    // let postsHTML = renderPosts({
    //   posts: postList
    // });
    // $('div#posts').html(postsHTML);
    renderPostList();
    // let postHTML = renderPost(response);
    // $('div#posts').prepend(postHTML);
    // $('div#posts').append(`
    //   <div class="post">
    //     <p>${response.createdAt}</p>
    //     <p>${response.body}</p>
    //   </div>
    // `);
  }, function(response) {
    console.log(response);
  });
});

$.ajax({
  type: 'GET',
  url: 'http://localhost:3000/api/posts'
}).then(function(response) {
  postList = response;
  renderPostList();
  // let postsHTML = renderPosts({
  //   posts: response
  // });
  // $('div#posts').html(postsHTML);
});

function renderPostList() {
  let postsHTML = renderPosts({
    posts: postList
  });
  $('div#posts').html(postsHTML);
}
